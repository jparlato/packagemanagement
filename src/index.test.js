import {expect} from 'chai';
import jsdom from 'jsdom';
import fs from 'fs';


const virtualConsole = jsdom.createVirtualConsole();
virtualConsole.sendTo(console);

// describe('Our first test', () => {
//     it('should pass', () => {
//         expect(true).to.equal(true);
//     })
// })

describe('index.html', () => {
    it('should say hello',  (done) => {
       const index = fs.readFileSync('./src/index.html', "utf-8");
       jsdom.env(index, function(err, window) {
           const h1 = window.document.getElementsByTagName('h1')[0];
           console.log('this is my log: ' + h1.innerHTML);
           expect(h1.innerHTML).to.contain('Hello World!');
           done();
           window.close();
       });
    })
})

describe('index.html', () => {
    it('should have h1 that says Hello', (done) => {
      const index = fs.readFileSync('./src/index.html', "utf-8");
      jsdom.env(index, function(err, window) {
        const h1 = window.document.getElementsByTagName('h1')[0];
        expect(h1.innerHTML).to.equal("Hello World!");
        done();
        window.close();
      });
    })
  })